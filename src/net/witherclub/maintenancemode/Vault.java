package net.witherclub.maintenancemode;

import org.bukkit.Server;
import org.bukkit.plugin.RegisteredServiceProvider;

import net.milkbowl.vault.permission.Permission;

public class Vault {
	
	public static Permission perms = null;
	
	public boolean setupVault(Server server) {
		RegisteredServiceProvider<Permission> rsp = server.getServicesManager().getRegistration(Permission.class);
		perms = rsp.getProvider();
		
		return perms != null;
	}
}
