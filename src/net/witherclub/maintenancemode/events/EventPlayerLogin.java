package net.witherclub.maintenancemode.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.witherclub.maintenancemode.MaintenanceMode;
import net.witherclub.maintenancemode.Vault;

public class EventPlayerLogin implements Listener {

	@EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
		if ( !Vault.perms.has(event.getPlayer(), "maintenance.canjoin") && !MaintenanceMode.enabled) {
			event.getPlayer().kickPlayer(MaintenanceMode.kick_message);
		}
	}
}
