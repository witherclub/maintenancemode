package net.witherclub.maintenancemode.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import net.witherclub.maintenancemode.MaintenanceMode;

public class EventServerListPing implements Listener {

	@EventHandler
    public void ServerListPing(ServerListPingEvent event) {
		if (!MaintenanceMode.enabled)
			event.setMotd(MaintenanceMode.motd);
    }
}
