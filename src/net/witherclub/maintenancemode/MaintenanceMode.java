package net.witherclub.maintenancemode;

import java.io.File;
import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import net.witherclub.maintenancemode.commands.CommandMaintenanceToggle;
import net.witherclub.maintenancemode.events.EventPlayerLogin;
import net.witherclub.maintenancemode.events.EventServerListPing;

public class MaintenanceMode extends JavaPlugin {
	
	public static Logger logger;
	public static FileConfiguration config;
	public static Vault vault = new Vault();
	
	public static boolean enabled;
	public static String motd;
	public static String kick_message;
	
	@Override
    public void onEnable() {
		logger = getServer().getLogger();
		config = this.getConfig();
		
		if (!vault.setupVault(getServer())) {
			this.setEnabled(false);
			logger.severe("Could not locate vault! You need vault in order to use this plugin.");
			logger.info("Plugin disabled!");
			return;
		}
		
		loadConfig();
		
		this.getCommand("maintenancetoggle").setExecutor(new CommandMaintenanceToggle());
		
		getServer().getPluginManager().registerEvents(new EventServerListPing(), this);
		getServer().getPluginManager().registerEvents(new EventPlayerLogin(), this);
	}
	
	private void loadConfig() {
		if (!getDataFolder().exists()) {
			getDataFolder().mkdirs();
		}
		
		File file = new File(getDataFolder(), "config.yml");
		if (!file.exists()) {
			logger.info("Config.yml not found, copying defaults!");
			saveDefaultConfig();
		}

		enabled = this.getConfig().getBoolean("general.enabled");
		
		motd = this.getConfig().getString("messages.motd");
		kick_message = this.getConfig().getString("messages.kick_message");
		
		motd = motd.replaceAll("&", "�");
		kick_message = kick_message.replaceAll("&", "�");
		
		logger.info("Loaded Config.yml!");
	}
}
