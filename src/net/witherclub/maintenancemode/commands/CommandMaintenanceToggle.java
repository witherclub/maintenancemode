package net.witherclub.maintenancemode.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.witherclub.maintenancemode.MaintenanceMode;
import net.witherclub.maintenancemode.Vault;

public class CommandMaintenanceToggle implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player) sender;
		
		if ( Vault.perms.has(player, "maintenance.admin") ) {
			if ( MaintenanceMode.enabled ) {
				player.sendMessage(ChatColor.GREEN + "Maintenance Mode is enabled!");
				MaintenanceMode.enabled = false;
				MaintenanceMode.config.set("general.enabled", false);
			} else {
				player.sendMessage(ChatColor.GREEN + "Maintenance Mode is disabled!");
				MaintenanceMode.enabled = true;
				MaintenanceMode.config.set("general.enabled", true);
			}
		} else {
			player.sendMessage(ChatColor.RED + "You do not have permission to use this command!");
		}
		
		return true;
	}

}
